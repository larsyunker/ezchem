from setuptools import setup, find_packages

NAME = 'ezchem'
VERSION = '0.1.0'
AUTHOR = 'Lars Yunker'

PACKAGES = find_packages()
# KEYWORDS = ', '.join([
# ])

# with open('LICENSE') as f:
#     license = f.read()
#     license.replace('\n', ' ')
#
# with open('README.MD') as f:
#     long_description = f.read()

setup(
    name=NAME,
    version=VERSION,
    description='Tools to simplify the life of the everyday chemist',
    # long_description=long_description,
    long_description_content_type='text/markdown',
    author=AUTHOR,
    url='https://gitlab.com/larsyunker/ezchem',
    packages=PACKAGES,
    # license=license,
    python_requires='>=3',
    classifiers=[
        'Programming Language :: Python :: 3',
        'License :: OSI Approved :: MIT License',
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: Chemistry',
        'Topic :: Scientific/Engineering :: Information Analysis',
        'Operating System :: OS Independent',
        'Natural Language :: English'
    ],
    # keywords=KEYWORDS,
    install_requires=[
        'pythoms',
        'unithandler==1.2.2',
    ],
)

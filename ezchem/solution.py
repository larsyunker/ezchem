"""
Several classes and methods that make chemistry calculations more convenient
"""
import warnings
from pythoms.molecule import Molecule
from unithandler.base import UnitFloat
# todo create mL, mol, etc. scalars for easy definition


class Dilution(object):
    def __init__(self,
                 ci=None,
                 vi=None,
                 cf=None,
                 vf=None,
                 volume_prefix='',
                 concentration_prefix='',
                 ):
        """
        Calculates the values of the expression ci*vi=cf*vf

        :param float ci: initial concentration (M)
        :param float vi: initial volume (L)
        :param float cf: final concentration (M)
        :param float vf: final volume (L)
        :param str volume_prefix: prefix for stored volume values
        :param str concentration_prefix: prefix for stored concentration values
        """
        self._initial_concentration = None
        self._final_concentration = None
        self._initial_volume = None
        self._final_volume = None

        # store incoming as attributes
        self._conc_prefix = {  # concentration prefixes
            'si_prefix': concentration_prefix,
            'stored_prefix': concentration_prefix,
        }
        self._vol_prefix = {  # volume prefixes
            'si_prefix': volume_prefix,
            'stored_prefix': volume_prefix
        }
        if ci is not None:
            self.initial_concentration = ci
        if cf is not None:
            self.final_concentration = cf
        if vi is not None:
            self.initial_volume = vi
        if vf is not None:
            self.final_volume = vf

    def __repr__(self):
        return f'{self.__class__.__name__}({self.ci}, {self.vi}, {self.cf}, {self.vf})'

    def __str__(self):
        return f'{self.__class__.__name__}(ci: {self.ci}, vi:{self.vi}, cf: {self.cf}, vf: {self.vf})'

    @property
    def initial_concentration(self) -> UnitFloat:
        """Initial concentration"""
        if all([self._final_concentration, self._initial_volume, self._final_volume]):
            return (
                self._final_concentration
                * self._final_volume
                / self._initial_volume
            )
        return self._initial_concentration

    @initial_concentration.setter
    def initial_concentration(self, value: float):
        self._initial_concentration = UnitFloat(
            value,
            'mol/L',
            **self._conc_prefix
        )

    @initial_concentration.deleter
    def initial_concentration(self):
        self._initial_concentration = None

    @property
    def final_concentration(self) -> UnitFloat:
        """final concentration for the dilution"""
        if all([self._initial_concentration, self._initial_volume, self._final_volume]):
            return (
                self._initial_concentration
                * self._initial_volume
                / self._final_volume
            )
        return self._final_concentration

    @final_concentration.setter
    def final_concentration(self, value: float):
        self._final_concentration = UnitFloat(
            value,
            'mol/L',
            **self._conc_prefix
        )

    @final_concentration.deleter
    def final_concentration(self):
        self._final_concentration = None

    @property
    def initial_volume(self) -> UnitFloat:
        """initial volume for the dilution"""
        if all([self._initial_concentration, self._final_concentration, self._final_volume]):
            return (
                self._final_concentration
                * self._final_volume
                / self._initial_concentration
            )
        return self._initial_volume

    @initial_volume.setter
    def initial_volume(self, value: float):
        self._initial_volume = UnitFloat(
            value,
            'L',
            **self._vol_prefix
        )

    @initial_volume.deleter
    def initial_volume(self):
        self._initial_volume = None

    @property
    def final_volume(self) -> UnitFloat:
        if all([self._initial_volume, self._initial_concentration, self._final_concentration]):
            return (
                self._initial_concentration
                * self._initial_volume
                / self._final_concentration
            )
        return self._final_volume

    @final_volume.setter
    def final_volume(self, value: float):
        self._final_volume = UnitFloat(
            value,
            'L',
            **self._vol_prefix
        )

    @final_volume.deleter
    def final_volume(self):
        self._final_volume = None

    @property
    def fill(self) -> UnitFloat:
        """volume difference for the dilution"""
        if all([self.final_volume, self.initial_volume]):
            return self.final_volume - self.initial_volume
        return None

    """legacy attribute retrieval"""
    @property
    def ci(self) -> UnitFloat:
        return self.initial_concentration

    @property
    def cf(self) -> UnitFloat:
        return self.final_concentration

    @property
    def vi(self) -> UnitFloat:
        return self.initial_volume

    @property
    def vf(self) -> UnitFloat:
        return self.final_volume

    def different_prefix(self, key: str, target_prefix: str) -> float:
        """
        Scales the value of the target key to the specified prefix. This can also be accomplished by calling
        `specific_prefix()` on the desired value.

        :param str key: defined value (ci, cf, vi, or vf)
        :param str target_prefix: target si prefix
        :return: scaled value
        :rtype: float
        """
        # todo deprecate to specific_prefix to keep consistent method name
        return self.__dict__[key].specific_prefix(target_prefix)

    def calc_fill(self) -> UnitFloat:
        """
        Calculates the volume required to fill with solvent to achieve the target concentration.

        :param vf: final volume (L)
        :param vi: initial volume (L)
        :return: volume difference
        :rtype: float
        """
        warnings.warn(
            'The calc_fill method has been deprecated, retrieve the fill attribute directly',
            DeprecationWarning,
            stacklevel=2,
        )
        return self.fill


_valid_units = [
    'g',
    'L',
    'mol',
    'mol/L',
    'M',
]


class Sample(object):
    def __init__(self,
                 amount,
                 amount_type='mass',
                 mw=None,
                 molecular_formula='',  # todo legacy catch formula
                 density=None,
                 dissolved_volume=None,  # todo legacy catch volume
                 ):
        """
        Data class that stores an amount of substance and its concentration in solution.

        :param float, UnitValue amount: amount of material. If a UnitValue is handed, the unit will be used to
            interpret the type of amount handed to the instance.
        :param 'mass', 'volume', 'moles', amount_type: the type of amount handed to the instance
        :param float mw: molecular weight of the molecule (g/mol)
        :param str molecular_formula: molecular formula of the molecule (the molecular weight will be calculated from this)
        :param float density: density of the material (if a liquid; g/mL)
        :param float dissolved_volume: total volume of the solution (mL)
        """
        self._concentration = None  # concentration (enabled after dissolution)
        self._volume = None  # volume that substance is dissolved in
        self._molecule = Molecule(molecular_formula)  # molecule instance
        self._density = None  # density of the substance
        self._n_mols = None  # number of moles

        # if provided with string, attempt auto-converstion to UnitFloat
        if type(amount) == str:
            amount = UnitFloat(amount)

        if isinstance(amount, UnitFloat):
            if amount.unit == 'g':  # if a mass
                amount_type = 'mass'
            elif amount.unit == 'L':  # if a volume
                amount_type = 'volume'
            elif amount.unit == 'mol':  # if a number of moles
                amount_type = 'moles'
            elif amount.units in [{'mol': 1, 'L': -1}, {'M': 1}]:  # concentration
                amount_type = 'concentration'
            else:
                raise ValueError(f'Unrecognized unit: "{amount.unit}". '
                                 f'Valid units: {", ".join(val for val in _valid_units)}')
            amount = amount.specific_prefix('')

        # set attributes
        if mw is not None:
            self.molecular_weight = mw
        self.molecular_formula = molecular_formula
        if dissolved_volume is not None:
            self.volume = dissolved_volume
        if density is not None:
            self.density = density
        if dissolved_volume is not None:
            self.volume = dissolved_volume

        if amount_type == 'mass':  # if handed a mass, convert to moles
            self.number_of_moles = amount / self.molecular_weight
        elif amount_type == 'volume':  # if handed a volume, need to convert using density
            if density is None:
                raise ValueError('If a volume is provided for an amount, density must be specified.')
            self.number_of_moles = amount * self.density / self.molecular_weight
        elif amount_type == 'moles':  # if handed moles
            self.number_of_moles = amount
        elif amount_type in ['conc', 'concentration']:  # if handed concentration
            self.concentration = amount
        else:
            raise ValueError(f'The amount_type "{amount_type}" is not recongnized.')

    def __repr__(self):
        return f'{self.__class__.__name__}({self.number_of_moles}, {self.concentration})'

    def __str__(self):
        out = f'{self.__class__.__name__}'
        if self.number_of_moles is not None:
            out += f' {self.number_of_moles}'
        if self.molecular_formula is not None:
            out += f' {self._molecule.molecular_formula}'
        if self.concentration is not None:  # append concentration if present
            out += f' at {self.concentration}'
        return out

    @property
    def concentration(self) -> UnitFloat:
        """concentration of the substance/solution"""
        if self._concentration is not None:
            return self._concentration
        elif all([self.number_of_moles, self.volume]):  # todo necessary since both values are UnitFloats anyway?
            return UnitFloat(  # store concentration
                self.number_of_moles / self.volume,
                'mol/L',
            )
        else:
            return None

    @concentration.setter
    def concentration(self, value):
        self._concentration = UnitFloat(  # store concentration
            value,
            'mol/L',
        )

    @concentration.deleter
    def concentration(self):
        self._concentration = None

    @property
    def volume(self) -> UnitFloat:
        """volume that the substance is dissolved in"""
        return self._volume

    @volume.setter
    def volume(self, value):
        self._volume = UnitFloat(
            value,
            'L',
            'm'
        )

    @volume.deleter
    def volume(self):
        self._volume = None

    @property
    def molecular_weight(self):
        """molecular weight of the substance"""
        if self._molecule is not None:
            mw = self._molecule.molecular_weight
        elif self._mw is not None:
            mw = self._mw
        else:
            return None
        return UnitFloat(
            mw,
            'g/mol'
        )

    @molecular_weight.setter
    def molecular_weight(self, value):
        self._mw = value

    @molecular_weight.deleter
    def molecular_weight(self):
        self._mw = None

    @property
    def molecular_formula(self) -> str:
        if self._molecule.molecular_formula == '':
            return None
        return self._molecule.molecular_formula

    @molecular_formula.setter
    def molecular_formula(self, value):
        self._molecule.molecular_formula = value

    @molecular_formula.deleter
    def molecular_formula(self):
        self._molecule.molecular_formula = ''

    @property
    def density(self) -> UnitFloat:
        """density of the compound (kg/L)"""
        return self._density

    @density.setter
    def density(self, value):
        self._density = UnitFloat(
            value * 1000.,  # need to scale up to match units
            'g/L',
            si_prefix='k',  # kg are not managed as units for unithandler, so scaling is required
            stored_prefix='k'
        )

    @density.deleter
    def density(self):
        self._density = None

    @property
    def number_of_moles(self) -> UnitFloat:
        """number of moles of the substance"""
        return self._n_mols

    @number_of_moles.setter
    def number_of_moles(self, value):
        self._n_mols = UnitFloat(
            value,
            'mol',
        )

    @number_of_moles.deleter
    def number_of_moles(self):
        self._n_mols = None

    @property
    def conc(self):
        """legacy retrieval of concentration"""
        warnings.warn(
            f"Retrieve 'concentration' instead of 'conc' from {self.__class__.__name__} instances",
            DeprecationWarning,
            stacklevel=2,
        )
        return self.concentration

    @property
    def mw(self):
        """legacy retrieval"""
        warnings.warn(
            f"Retrieve 'molecular_weight' instead of 'mw' from {self.__class__.__name__} instances",
            DeprecationWarning,
            stacklevel=2,
        )
        return self.molecular_weight

    @property
    def mol(self):
        """legacy retrieval"""
        warnings.warn(
            f"Retrieve 'number_of_moles' instead of 'mol' from {self.__class__.__name__} instances",
            DeprecationWarning,
            stacklevel=2,
        )
        return self.number_of_moles

    def dissolve(self, volume):
        """
        Dissolves the substance in the specified volume, activating the conc class attribute.

        :param volume: volume used to dissolve (mL)
        :return: concentration (M)
        :rtype: UnitFloat
        """
        warnings.warn(
            f"The dissovle method of {self.__class__.__name__} instances is deprecated, "
            f"modify the volume attribute directly",
            DeprecationWarning,
            stacklevel=2,
        )
        if isinstance(volume, UnitFloat):  # if UnitValue, scale
            volume = volume.specific_prefix('')
        self.volume = volume
        return self.concentration

    def dilute(self, volume):
        """
        Dilutes the concentration to the specified volume

        :param float, UnitValue volume: new volume (mL)
        :return: new concentration (M)
        """
        if self.volume is None:  # if amount has not been dissolved yet, call instead
            self.volume = volume
            return

        # todo figure out how to monitor total number of moles in the sample
        self.concentration = Dilution(  # calculate dilution and store new concentration
            vi=self.volume,
            ci=self.concentration,
            vf=volume,
        ).cf
        self.volume = volume  # set new volume
        return self.concentration

    def reduce_volume(self, volume):
        """
        Reduces the tracked volume to the specified volume.

        :param volume: new volume (mL)
        :return: new volume (mL)
        """
        if isinstance(volume, UnitFloat):  # if UnitValue, scale
            volume = volume.specific_prefix('m')
        volume = UnitFloat(
            volume,
            'L',
            'm'
        )
        self.volume = volume


class SubstanceDaemon(object):
    def __init__(self,
                 *incoming,
                 current_volume=0.,
                 ):
        """
        A daemon for managing the contents of a container. (Several substances contained sharing the same final volume).

        :param current_volume: The volume that the container will/does contain (mL)
        :param incoming: incoming keyword argument sets for add_substance calls
        """
        # todo modify to use new Sample functionality
        self.current_volume = UnitFloat(current_volume, 'L', 'm', 'm')
        self.tracker = {}
        for inc in incoming:
            if type(inc) != dict:
                raise TypeError(
                    f'Incoming kwarg value to {self.__class__.__name__} is not a dictionary. Type: {type(inc)}')
            self.add_substance(**inc)
        # todo store solvent type?
        # todo convert current volume to a property (as in container)

    def __repr__(self):
        return f'{self.__class__.__name__}({len(self)}, {self.current_volume})'

    def __str__(self):
        return f'{self.__class__.__name__}({len(self)} substances in {self.current_volume})'

    def __getitem__(self, item):
        return self.tracker[item]

    # def __getattr__(self, item):
    #     if item in self.tracker:
    #         return self.tracker[item]
    #     return self.__dict__[item]

    def __iter__(self) -> Sample:
        for item in self.tracker:
            yield self.tracker[item]

    def __len__(self):
        return len(self.tracker)

    def add_substance(self,
                      name,
                      dispensed=False,
                      **substance_kwargs,
                      ):
        """
        Stores the amount of substance in the contents tracker.

        :param str name: convenience name for retrieval
        :param dispensed: whether the substance has been dispensed
        :param substance_kwargs: keyword arguments for initialization of a Substance instance
        """
        if name in self.tracker:
            raise KeyError(f'The substance {name} is already defined in the tracker instance.')

        self.tracker[name] = Sample(
            **substance_kwargs,
        )
        self.tracker[name].dispensed = dispensed  # store dispensed bool
        if dispensed is True and self.current_volume != 0.:  # dissolve if there is volume
            self.tracker[name].dissolve(self.current_volume)

    def dispensed(self, name):
        """
        Sets the flag denoting a substance's dispensed status to True.

        :param name: name of the substance
        """
        self.tracker[name].dispensed = True

    def dissolve(self, volume):
        """
        Dissolves the substances stored in the daemon in the volume specified.

        :param float or UnitFloat volume: volume (mL)
        """
        self.current_volume += volume
        for substance in self:
            substance.dissolve(volume)

    def remove_volume(self, volume):
        """
        Removes volume from the volume tracker.

        :param float, UnitValue volume: volume removed (mL)
        :return: current volume
        """
        if self.current_volume - volume < 0.:
            raise ValueError(
                f'The removed volume will be below that stored in the volume tracker ({self.current_volume})')
        self.current_volume -= volume
        for substance in self:
            substance.reduce_volume(self.current_volume)
        return self.current_volume

    def add_volume(self, volume):
        """
        Adds volume to the volume tracker.

        :param float, UnitValue volume: volume added (mL)
        :return: current volume
        """
        self.current_volume += volume
        for substance in self:
            substance.dilute(self.current_volume)

    def iterate_undispensed(self):
        """
        Returns a generator that iterates over the substances in the tracker, returning only substances that have not
        been dispensed.

        :return: undispensed substances
        :rtype: generator
        """
        for substance in self:
            if substance.dispensed is False:
                yield substance


def Substance(*args, **kwargs):
    """Catch for deprecated Substance class"""
    warnings.warn(
        f"The Substance class has been renamed to Sample, import that instead",
        DeprecationWarning,
        stacklevel=2,
    )
    return Sample(*args, **kwargs)
